<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    protected $fillable = [
        'product_name',
        'free_service_year',
        'service_year',
        'service_interval_time',
        'rate',
        'sales_date',
        'service_charges'
    ];
}
