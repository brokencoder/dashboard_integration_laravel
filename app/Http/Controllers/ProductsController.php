<?php

namespace App\Http\Controllers;

use App\products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class ProductsController extends Controller
{

    public function displayProductData()
    {
        $data = products::latest()->get();
        return Datatables::of($data)
            ->make(true);
    }

    public function index(products $products)
    {
        return view('products.index', ['products'=>$products::all()]);
    }

    public function create()
    {
        return view('products.create');
    }

    public function store(Request $request)
    {
        $custom_error_messages = [
            'product-name.required'=>'You must provide product name',
            'product-code.required'=>'You must provide product code',
            'product-created-date.required'=>'You must provide product added date',
            'product-rate.required'=> 'You must provide product price',
            'product-code.unique'=>'You must new product code',
            'product-availability.required'=>'Please select the availability',
        ];
        $validator = Validator::make($request->input(), [
            'product-name'=>'required',
            'product-created-date'=>'required',
            'product-rate'=>'required',
            'product-availability'=> 'required',
            'product-code'=>'required|unique:products,product_code'
        ], $custom_error_messages);

        if ($validator->fails())
        {
            return redirect('products/create')->withErrors($validator)->withInput();
        }
        else
        {
                $product = new products;
                $product->product_name = $request->input('product-name');
                $product->product_code = $request->input('product-code');
                $product->product_created_data = $request->input('product-created-date');
                $product->rate = $request->input('product-rate');
                $product->status = $request->input('product-availability')=='Yes'? 1:0;
                $product->save();
                return redirect()->route('products.index')->withStatus(__('Products successfully created.'));
        }
    }

    public function edit(Request $request,int $id)
    {
        $product = Products::find($id);
        if ($product)
        {
            return view('products.edit', ['product'=>$product]);
        }
        else{
            return redirect('products/index');
        }
    }
    public function update(Request $request, int $id)
    {
        $custom_error_messages = [
            'product-name.required'=>'You must provide product name',
            'product-code.required'=>'You must provide product code',
            'product-created-date.required'=>'You must provide  product adding date',
            'product-rate.required'=> 'You must provide product price',
            'product-code.unique'=>'You must provide new product code',
            'product-availability.required'=>'Please select the availability',
        ];
        $validator = Validator::make($request->input(), [
            'product-name'=>'required',
            'product-created-date'=>'required',
            'product-rate'=>'required',
            'product-availability'=> 'required',
            'product-code'=>'required|unique:products,product_code,'.$id,
        ], $custom_error_messages);

        if ($validator->fails())
        {
            return redirect('products/modify/'.$id)->withErrors($validator)->withInput();
        }
        else
        {
            $product = Products::find($id);
            if ($product) {

                $product->product_name = $request->input('product-name');
                $product->product_code = $request->input('product-code');
                $product->product_created_data = $request->input('product-created-date');
                $product->rate = $request->input('product-rate');
                $product->status = $request->input('product-availability') == 'Yes' ? 1 : 0;
                $product->save();
                return redirect('products/index')->withStatus(__('Product update successfully.'));
            }
            else
            {
                return redirect('products/index');
            }
        }
    }

    public function delete($id)
    {
        $product = products::find($id);
        if ($product){
            $product->delete();
            return redirect('products/index')->withStatus(__('Product deleted successfully'));
        }
        else
        {
            return redirect('products/index');
        }
    }

    public function displayProductInfomation($product_id)
    {
        $product = Products::find($product_id);

        if ($product)
        {
            return view('products.product_view', ['product'=>$product]);
        }
        else{
            return view('products.index');
        }
    }
}
