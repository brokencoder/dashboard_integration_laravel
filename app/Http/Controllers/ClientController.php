<?php

namespace App\Http\Controllers;

use App\Clients;
use App\ClientsServices;
use App\Events\PushServiceDisplayNotification;
use App\Notifications\DummyNotification;
use App\products;
use App\Services;
use Carbon\Carbon;
use App\User;
use Cassandra\Date;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use MongoDB\Driver\Session;
use phpDocumentor\Reflection\DocBlock\Description;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use App\Exceptions\Handler;
use Yajra\DataTables\Facades\DataTables;

class ClientController extends Controller
{
    public function displayClientData(Request $request)
    {

            $data = Clients::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">View</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
    }

    public function index(Request $request)
    {

        $clients = Clients::all();
        return view('client.index', ['clients'=>$clients]);
    }

    public function create()
    {
        return view('client.create');
    }

    public function store(Request $request)
    {
        $custom_error_messages = [
            'input-email-id.required' => 'We need provide your e-mail address!',
            'input-email-id.unique'=> 'Your e-mail must be unique',
            'input-email-id.email'=> 'You must provide a valid e-mail address',
            'first-name.required'=> 'You must provide a valid first name',
            'last-name.required'=> 'You must provide a valid last name',
            'input-address.required'=> 'You must provide a your address',
            'input-phone-number.required'=> 'You must provide a your phone number',
            'input-phone-number.unique'=> 'Phone number already exists  ',
            'input-phone-number.digits'=> 'Invalid phone number',
        ];
        $validator = Validator::make($request->input(), [
            'first-name'=> 'required|string|max:100|min:2',
            'last-name'=> 'required|string|max:100|min:2',
            'input-address'=> 'required|string|max:200|min:5',
            'input-phone-number'=> 'required|integer|unique:clients,phone_number|digits:10,',
            'input-email-id'=>'required|email|unique:clients,email'
        ], $custom_error_messages);

        if ($validator->fails())
        {
            return redirect('client/create')->withErrors($validator)->withInput();
        }
        else
        {
            $client = new Clients;
            $client->first_name = $request->input('first-name');
            $client->last_name = $request->input('last-name');
            $client->address = $request->input('input-address');
            $client->phone_number = $request->input('input-phone-number');
            $client->email = $request->input('input-email-id');
            $client->save();
            return redirect('client/index')->withStatus(__('Client created successfully.'));
        }
    }

    public function edit(Request $request,int $id)
    {
            $client = Clients::find($id);
            if ($client)
            {
                return view('client.edit', ['client'=>$client]);
            }
            else{
                return redirect('client/index');
            }
    }
    public function update(Request $request, int $id )
    {
        $custom_error_messages = [
            'input-email-id.required' => 'We need provide your e-mail address!',
            'input-email-id.unique'=> 'Your e-mail must be unique',
            'input-email-id.email'=> 'You must provide a valid e-mail address',
            'first-name.required'=> 'You must provide a valid first name',
            'last-name.required'=> 'You must provide a valid last name',
            'input-address.required'=> 'You must provide a your address',
            'input-phone-number.required'=> 'You must provide a your phone number',
            'input-phone-number.unique'=> 'Phone number already exists  ',
            'input-phone-number.digits'=> 'Invalid phone number',
            'input-phone-number.integer'=> 'Invalid phone number',
            'input-phone-number.max'=> 'Invalid phone number',
        ];
        $validator = Validator::make($request->input(), [
            'first-name'=> 'required|string|max:100|min:2',
            'last-name'=> 'required|string|max:100|min:2',
            'input-address'=> 'required|string|max:200|min:5',
            'input-phone-number'=> 'required|unique:clients,phone_number,'.$id.'|integer|digits:10,max:10',
            'input-email-id'=> 'required|email|unique:clients,email,'.$id,
        ], $custom_error_messages);
//
        if ($validator->fails())
        {

            return redirect('client/modify/'.$id)->withErrors($validator)->withInput();
        }
        else
        {
            $client = Clients::where('id', $id)->get()[0];
            $client->first_name = $request->input('first-name');
            $client->last_name = $request->input('last-name');
            $client->address = $request->input('input-address');
            $client->phone_number = $request->input('input-phone-number');
            $client->email = $request->input('input-email-id');
            $client->save();
            return redirect('client/index')->withStatus(__('Client updated successfully.'));
        }
    }

    public function delete(Request $request, int $client_id, int $service_id=null )
    {

        if (str_contains($request->url(), 'services'))
        {
           Services::find($service_id)->delete();
           return "deleted";
        }
        elseif (!str_contains($request->url(), 'services'))
        {
            Clients::find($client_id)->delete();
            return "deleted";
        }
            return redirect('client/index')->withStatus(__('Client deleted successfully.'));
    }

    public function display_services(Request $request, int $client_id)
    {
        $client = Clients::find($client_id);
        if ($client)
        {
            $services = Services::where('clients_id', $client_id)->get();
            $products = products::all();
            return view('client/add_services')->with(['products'=>$products,'client_id'=>$client_id, 'client'=>$client, 'services'=>$services]);
        }
        else
        {
            return view('/dashboard');
        }

    }
    public function add_service_data(Request $request, int $client_id)
    {

        $service_data = array();
        if ($request->input('row-count')==null)
        {
            return redirect('/dashboard');
        }
        else
        {
            for ($i=1;$i<=$request->input('row-count');$i++)
            {

                $validator = Validator::make($request->input(), [
                    'product-name-'.$i=> 'required',
                    'product-service-year-'.$i=> 'required|integer',
                    'product-free-service-year-'.$i=> 'required|integer',
                    'product-interval-time-'.$i=>'required|integer',
                    'product-rate-'.$i=> 'required|integer',
                    'product-sales-date-'.$i=>'required|date|date_format:Y-m-d',
                    'product-service-charge-'.$i=>'required|integer'
                ]);

                if ($validator->fails())
                {
                    $request->session()->put('service-errors', 'Please fill the details correctly');
                    return redirect('/client/add_services/'.$client_id)->withInput($request->input());
                }
                else{
                    array_push($service_data,
                        array(
                            'clients_id'=>$client_id,
                            'products_id'=>$request->input('product-name-'.$i),
                            'free_service_year'=>$request->input('product-free-service-year-'.$i),
                            'service_year'=>$request->input('product-service-year-'.$i),
                            'rate'=>$request->input('product-rate-'.$i),
                            'sales_date'=>$request->input('product-sales-date-'.$i),
                            'service_charges'=>$request->input('product-service-charge-'.$i),
                            'service_interval_time'=>$request->input('product-interval-time-'.$i))
                        );
                    }
            }
            Services::insert($service_data);
            $request->session()->forget('service-errors');
            return redirect('/client/add_services/'.$client_id);
        }
   }

    public function sendNotification()
    {
        event(new PushServiceDisplayNotification('pratik'));
    }

    public function client_added_services(Request $request, int $client_id=null, int $service_id=null)
    {
        $url_result = str_contains($request->url(), 'edit');
        switch ($url_result) {
            case true:

                $client_services = Services::where('clients_id', $client_id)->where('id', $service_id)->first();
                if ($client_services)
                {
                    $custom_error_messages = [
                        'product-service-year-'.$service_id.'required' => 'You need provide service year!',
                        'product-free-service-year-'.$service_id.'required'=> 'You need provide free service year',
                        'product-interval-time-'.$service_id.'required'=> 'You need provide interval time',
                        'product-rate-'.$service_id.'required'=> 'You must provide rate',
                        'product-sales-date-' . $service_id.'required'=> 'You must provide a sales date',
                        'product-service-charge-' . $service_id.'required'=> 'You must provide service charge',
                        'product-service-year-'.$service_id.'date' => 'We need provide valid service year!',
                        'product-free-service-year-'.$service_id.'integer'=> 'You need provide valid free service year',
                        'product-interval-time-'.$service_id.'interger'=> 'You need provide valid interval time',
                        'product-rate-'.$service_id.'integer'=> 'You need provide valid rate value',
                        'product-sales-date-' . $service_id.'date'=> 'You must provide a valid sales date',
                        'product-service-charge-' . $service_id.'required'=> 'You must provide valid service charge',

                    ];

                    $validator = Validator::make($request->input(), [
                        'product-service-year-' . $service_id => 'required|integer',
                        'product-free-service-year-' . $service_id => 'required|integer',
                        'product-interval-time-' . $service_id => 'required|integer',
                        'product-rate-' . $service_id => 'required|integer',
                        'product-sales-date-' . $service_id => 'required',
                        'product-service-charge-' . $service_id => 'required|integer'
                    ], $custom_error_messages);

                    if ($validator->fails())
                    {

//                        dd($validator->errors());
                        return Redirect::back()->withErrors($validator)->withInput();
                    }
                    else{

                        $client_services->products_id = $request->input('product-name-'.$service_id);
                        $client_services->free_service_year = $request->input('product-free-service-year-'.$service_id);
                        $client_services->service_year = $request->input('product-service-year-'.$service_id);
                        $client_services->rate = $request->input('product-rate-'.$service_id);
                        $client_services->sales_date = $request->input('product-sales-date-'.$service_id);
                        $client_services->service_charges = $request->input('product-service-charge-'.$service_id);
                        $client_services->service_interval_time = $request->input('product-interval-time-'.$service_id);
                        $client_services->save();
                    }
                }
                redirect('/dashboard');
                break;
            case false:
                // display logic

//                $service_data = array();
                $client_services = Services::where('clients_id', $client_id)->where('id', $service_id)->first();

                if ($client_services)
                {
                $products = Products::all();
                    return view('client.edit_added_services', ['client_id'=>$client_id,'service'=>$client_services,'products'=>$products]);
                }
                redirect('/dashboard');
                break;
            default:
               // redirect to dashboard

                redirect('/dashboard');
                break;
        }
    }

    public function displayClientServices(Request $request, int $client_id)
    {

            //returning services of client

            $services = Clients::find($client_id)->clientServices()->get();

            return view('client.display_services', ['services'=>$services]);
    }
    public  function displayClientInformatioin(Request $request, int $client_id)
    {
        $client = Clients::find($client_id);
        $product_ids = $client->clientServices()->select('products_id')->get();
        $products = DB::table('products')->whereIn('id', $product_ids)->get();
        return view('client.view', ['client'=>$client, 'products'=>$products]);
    }

}
