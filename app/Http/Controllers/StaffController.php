<?php

namespace App\Http\Controllers;
use App\Products;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Staff;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class StaffController extends Controller
{
    public function displayStaffData(Request $request)
    {
        $data = Staff::latest()->get();
        return Datatables::of($data)
            ->make(true);
    }
    public function index(Request $request)
    {
        $staff = Staff::all();
        return view('staff.index', ['staffs'=>$staff]);
    }

    public function create()
    {
        return view('staff.create');
    }

    public function store(Request $request)
    {
        $custom_error_messages = [
            'input-email-id.required' => 'We need provide your e-mail address!',
            'input-email-id.unique'=> 'Your e-mail must be unique',
            'input-email-id.email'=> 'You must provide a valid e-mail address',
            'first-name.required'=> 'You must provide a valid first name',
            'last-name.required'=> 'You must provide a valid last name',
            'input-address.required'=> 'You must provide a your address',
            'input-phone-number.integer'=> 'Invalid phone number',
            'input-phone-number.required'=> 'You must provide a your phone number',
            'input-phone-number.unique'=> 'Phone number already exists  ',
        ];
        $validator = Validator::make($request->input(), [
            'first-name'=> 'required|string|max:100|min:2',
            'last-name'=> 'required|string|max:100|min:2',
            'input-address'=> 'required|string|max:200|min:5',
            'input-phone-number'=> 'required|integer|unique:staff,phone_number|digits:10,',
            'input-email-id'=>'required|email|unique:staff,email'
        ], $custom_error_messages);

        if ($validator->fails())
        {
            return redirect('staff/create')->withErrors($validator)->withInput();
        }
        else
        {
            $client = new Staff;
            $client->first_name = $request->input('first-name');
            $client->last_name = $request->input('last-name');
            $client->address = $request->input('input-address');
            $client->phone_number = $request->input('input-phone-number');
            $client->email = $request->input('input-email-id');
            $client->save();
            return redirect('staff/index')->withStatus(__('Staff created successfully.'));
        }
    }

    public function edit(Request $request,int $staff_id)
    {
        $staff = Staff::find($staff_id);
        if ($staff)
        {
            return view('staff.edit', ['staff'=>$staff]);
        }
        else{
            return redirect('staff/index');
        }
    }

    public function update(Request $request, int $staff_id )
    {
        $custom_error_messages = [
            'input-email-id.required' => 'We need provide your e-mail address!',
            'input-email-id.unique'=> 'Your e-mail must be unique',
            'input-email-id.email'=> 'You must provide a valid e-mail address',
            'first-name.required'=> 'You must provide a valid first name',
            'last-name.required'=> 'You must provide a valid last name',
            'input-address.required'=> 'You must provide a your address',
            'input-phone-number.required'=> 'You must provide a your phone number',
            'input-phone-number.unique'=> 'Phone number already exists  ',
            'input-phone-number.digits'=> 'Invalid phone number',
            'input-phone-number.max'=> 'Invalid phone number',
        ];
        $validator = Validator::make($request->input(), [
            'first-name'=> 'required|string|max:100|min:2',
            'last-name'=> 'required|string|max:100|min:2',
            'input-address'=> 'required|string|max:200|min:5',
            'input-phone-number'=> 'required|unique:staff,phone_number,'.$staff_id.'|integer|digits:10,max:10',
            'input-email-id'=> 'required|unique:staff,email,'.$staff_id.'|email'
        ], $custom_error_messages);

        if ($validator->fails())
        {

            return Redirect::back()->withErrors($validator)->withInput();
        }
        else
        {
            $client = Staff::where('id', $staff_id)->get()[0];
            $client->first_name = $request->input('first-name');
            $client->last_name = $request->input('last-name');
            $client->address = $request->input('input-address');
            $client->phone_number = $request->input('input-phone-number');
            $client->email = $request->input('input-email-id');
            $client->save();
            return redirect('staff/index')->withStatus(__('Staff updated successfully.'));
        }
    }
    public function delete(int $id)
    {
        $client = Staff::where('id', $id)->delete();
        return redirect('staff/index')->withStatus(__('Staff deleted successfully.'));
    }

    function displayStaffInformatioin(int $staff_id)
    {
        $staff = Staff::find($staff_id);

        if ($staff)
        {
            return view('staff.staff_view', ['staff'=>$staff]);
        }
        else
        {
            return redirect('staff/index');
        }
    }
}
































