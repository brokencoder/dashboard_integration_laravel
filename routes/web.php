<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Events\PushServiceDisplayNotification;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function (){
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/profile/edit', 'ProfileController@edit')->name('profile.edit');
    Route::get('/profile/update', 'ProfileController@update')->name('profile.update');
    Route::get('/profile/password', 'ProfileController@password')->name('profile.password');
    Route::get('/user/index', 'UserController@index')->name('user.index');
    Route::post('/user/store', 'UserController@store')->name('user.store');
    Route::get('/user/create', 'UserController@create')->name('user.create');
    Route::get('/pages/icon', 'PageController@icon ')->name('pages.icons');
    Route::get('/pages/maps', 'PageController@maps ')->name('pages.maps');
    Route::get('/pages/notifications', 'PageController@notifications ')->name('pages.notifications');
    Route::get('/pages/tables', 'PageController@tables')->name('pages.tables');
    Route::get('/pages/typography', 'PageController@typography ')->name('pages.typography');
    Route::get('/pages/rtl', 'PageController@rtl ')->name('pages.rtl');
    Route::get('/pages/upgrade', 'PageController@upgrade ')->name('pages.upgrade');

    Route::get('/client/index', 'ClientController@index')->name('client.index');
    Route::post('/client/store', 'ClientController@store')->name('client.store');
    Route::get('/client/create', 'ClientController@create')->name('client.create');
    Route::get('/client/edit/{client_id}', 'ClientController@edit')->name('client.edit');
    Route::post('/client/update/{client_id}', 'ClientController@update')->name('client.update');
    Route::get('/client/delete/{client_id}', 'ClientController@delete');
    Route::get('/client/add_services/{client_id}', 'ClientController@display_services')->name('client.add_services');
    Route::post('/client/{client_id}/send_service_data', 'ClientController@add_service_data')->name('client.send_service_data');
    Route::get('/client/send_notification', 'ClientController@sendNotification');
    Route::get('/client/{client_id}/service/{service_id}/display', 'ClientController@client_added_services')->name('client.services.edit.display');
    Route::post('/client/{client_id}/services/{service_id}/edit', 'ClientController@client_added_services')->name('client.services.edit');
    Route::get('/client/{client_id}/services/{service_id}}', 'ClientController@client_added_services');
    Route::get('/client/delete/{client_id}/services/{service_id}', 'ClientController@delete');
    Route::get('/client/services/{client_id}', 'ClientController@displayClientServices');
    Route::get('/client/{client_id}/view', 'ClientController@displayClientInformatioin')->name('client.view');

    Route::get('/products/index', 'ProductsController@index')->name('products.index');
    Route::post('/product/store', 'ProductsController@store')->name('product.store');
    Route::get('/product/create', 'ProductsController@create')->name('product.create');
    Route::get('/product/edit/{product_id}', 'ProductsController@edit')->name('product.edit');
    Route::post('/product/update/{product_id}', 'ProductsController@update')->name('products.update');
    Route::get('/product/delete/{product_id}', 'ProductsController@delete')->name('products.delete');
    Route::get('/product/{product_id}/view', 'ProductsController@displayProductInfomation')->name('product.view');

    Route::get('/staff/index', 'StaffController@index')->name('staff.index');
    Route::post('/staff/store', 'StaffController@store')->name('staff.store');
    Route::get('/staff/create', 'StaffController@create')->name('staff.create');
    Route::get('/staff/edit/{staff_id}', 'StaffController@edit')->name('staff.edit');
    Route::post('/staff/update/{staff_id}', 'StaffController@update')->name('staff.update');
    Route::get('/staff/delete/{staff_id}', 'StaffController@delete')->name('staff.delete');
    Route::get('/staff/{staff_id}/view', 'StaffController@displayStaffInformatioin')->name('staff.view');



    Route::get('create', 'DisplayDataController@create')->name('display_data_controller_create');
    Route::get('index', 'ClientController@index')->name('display_data_controller_index');
    Route::get('display_client_data', 'ClientController@displayClientData')->name('displayclientdata');
    Route::get('display_product_data', 'ProductsController@displayProductData')->name('displayProductData');
    Route::get('display_staff_data', 'StaffController@displayStaffData')->name('displayStaffData');
    Route::view('/notification', 'notification');


});
Route::get('get_csrf_token', function(){
    return response()->json(['data'=>csrf_token()]);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
