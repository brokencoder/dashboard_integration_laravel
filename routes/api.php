<?php

use App\User;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return response()->json([
        'user'=>$request->user()
    ]);
});

Route::middleware('auth:api')->post('/register', 'Auth\RegisterController@register');

Route::middleware('auth:api')->get('/client/services/{id}', 'ClientController@displayClientServices');
