@extends('layouts.app', ['page' => __('Client Management'), 'pageSlug' => 'client'])

@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <object class="icon-gradient" data="{{URL::asset('icons/fonts/client_index_icon.svg')}}" style="margin-top: -3px; margin-left: -7px;" type="image/svg+xml" width="50" height="50"></object>
                    </div>
                    <div>Client Profile
                        <div class="page-title-subheading">Information relevant to client
                        </div>
                    </div>
                </div>
                <div class="page-title-actions">
                    <a href="{{url('client/index')}}" type="button" class="btn-shadow mr-3 btn btn-dark">
                        <i class="fas mr-1 fa-arrow-left"></i> Back to list
                    </a>
                </div>
            </div>
        </div>
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="card user-card-full">
                            <div class="row m-l-0 m-r-0">
                                <div class="col-sm-4 bg-c-lite-green user-profile">
                                    <div class="card-block text-center text-white">
                                        <div class="m-b-25">
                                            <img src="{{URL::asset ('images/user_profiles.png')}}" width="200px" alt="User-Profile-Image">
                                        </div>
                                        <h6 style="font-size: 2rem;">{{$client->first_name.' '.$client->last_name}}</h6>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="card-block">
                                        <h6 class="m-b-20 p-b-5 b-b-default f-w-600">Personal Information</h6>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p class="m-b-10 f-w-600">Email</p>
                                                <h6 class="text-muted f-w-400">{{$client->email}}</h6>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="m-b-10 f-w-600">Phone</p>
                                                <h6 class="text-muted f-w-400">{{$client->phone_number}}</h6>
                                            </div>
                                        </div>
                                        <div class="row mt-4">
                                            <div class="col-sm-6">
                                                <p class="m-b-10 f-w-600">Address</p>
                                                <h6 class="text-muted f-w-400">{{$client->address}}</h6>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="m-b-10 f-w-600">Total Products</p>
                                                <h6 class="text-muted f-w-400">{{count($products)}} </h6>
                                            </div>
                                        </div>
                                        <h6 class="m-b-20 m-t-40 p-b-5 b-b-default f-w-600">Product Information</h6>
                                        <div class="table-responsive">
                                            <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th class="text-center">No.</th>
                                                    <th class="text-center">Product Name</th>
                                                    <th class="text-center">Coming Service Date</th>
                                                </tr>
                                                </thead>
                                            <tbody>
                                            @for($i=0;$i<count($products);$i++)
                                                <tr>
                                                <td class="text-center text-muted"># {{$i+1}}</td>
                                                <td class="text-center text-muted">{{$products[$i]->product_name}}</td>
                                                <td class="text-center text-muted">{{$products[$i]->product_created_data}}</td>
                                            </tr>
                                            @endfor
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
