@extends('layouts.app', ['page' => __('Clients Management'), 'pageSlug' => 'client'])

@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <object class="icon-gradient" data="{{URL::asset('icons/fonts/client_index_icon.svg')}}" style="margin-top: -3px; margin-left: -7px;" type="image/svg+xml" width="50" height="50"></object>
                </div>
                <div>Client Dashboard
                    <div class="page-title-subheading">information about the clients
                    </div>
                </div>
            </div>
            <div class="page-title-actions">
                <a   href="{{url('client/create')}}" type="button" class="btn-shadow mr-3 btn btn-dark">
                    <i class="fas mr-2  fa-user-plus"></i>  New Client
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-header">Active Clients
                    <div class="btn-actions-pane-right">
                        <div role="group" class="btn-group-sm btn-group">
{{--                                <button class="active btn btn-focus">Last Week</button>--}}
{{--                                <button class="btn btn-focus">All Month</button>--}}
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">Sr. No</th>
                            <th>Full Name</th>
                            <th class="text-center">Phone</th>
{{--                            <th class="text-center">Status</th>--}}
                            <th class="text-center">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(sizeof($clients))
                            @for($i=0;$i<count($clients);$i++)
                        <tr>
                            <td class="text-center text-muted"># {{$i+1}}</td>
                            <td>
                                <div class="widget-content p-0">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left mr-3">
                                            <div class="widget-content-left">
                                                <img width="30" class="rounded-circle" src="{{url('images/avatars/worker.png')}}" alt="">
                                            </div>
                                        </div>
                                        <div class="widget-content-left flex2">
                                            <div class="widget-heading">{{$clients[$i]->first_name.' '.$clients[$i]->last_name }}</div>
{{--                                            <div class="widget-subheading opacity-7">Web Developer</div>--}}
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="text-center">{{$clients[$i]->phone_number}}</td>
{{--                            <td class="text-center">--}}
{{--                                <div class="badge badge-warning">Pending</div>--}}
{{--                            </td>--}}
                            <td class="text-center">
                                <a type="button" href="{{route('client.add_services', ['id'=>$clients[$i]->id])}}" class="btn  btn-secondary text-white btn-sm"> <i class="fas mr-1 fa-plus" aria-hidden="true"></i> Services</a>
                                <a type="button" href="{{route('client.view', ['client_id'=>$clients[$i]->id])}}" class="btn  btn-primary text-white btn-sm"> <i class="fas mr-1 fa-eye" aria-hidden="true"></i> View</a>
                                <a type="button" href="{{route('client.edit', ['client_id'=>$clients[$i]->id])}}" class="btn  btn-warning text-white btn-sm"> <i class="fas mr-1 fa-edit" aria-hidden="true"></i> Edit</a>
                                <a type="button" onclick="delete_client({{$clients[$i]->id}})" class="btn btn-danger text-white btn-sm"> <i class="fas fa-lg fa-trash fa-1x mr-1" aria-hidden="true"></i> Delete</a>
                            </td>
                        </tr>
                        @endfor
                        @endif

                        </tbody>
                    </table>
                </div>
{{--                <div class="d-block text-center card-footer">--}}
{{--                    <button class="mr-2 btn-icon btn-icon-only btn btn-outline-danger"><i class="pe-7s-trash btn-icon-wrapper"> </i></button>--}}
{{--                    <button class="btn-wide btn btn-success">Save</button>--}}
{{--                </div>--}}
            </div>
        </div>
    </div>
</div>

@section('js_script')
    <script>
    // displaying Client's data in data-table form
        $('#client-table').DataTable({});

        function  delete_client(id) {
            var csrf_token=$('meta[name="csrf_token"]').attr('content');
            swal({
                title: "Delete?",
                text: "You really like to delete this client ?",
                icon: "warning",
                buttons: ["cancel", "Delete"],
                dangerMode: true,
                })
                .then((willDelete) => {
                    if(willDelete){
                        $.ajax({
                            url : "{{ url('client/delete/')}}"+"/"+id,
                            type : "GET",
                            data : {'_token' :csrf_token},
                            success: function(data){
                                swal("Staff is deleted", {
                                    icon: "success",
                                    buttons: false,
                                    timer: 3000,
                                });

                            },
                            error : function(){
                                swal({
                                    title: 'Opps...',
                                    text : data.message,
                                    type : 'error',
                                    timer : '1500'
                                })
                            }
                        })
                    }else{

                    }
                })
            }

                    // function showModal(id){
                    //     $('#servicesModal').modal('toggle');
                        {{--var csrf_token=$('meta[name="csrf_token"]').attr('content');--}}
                        {{--$.ajax({--}}
                        {{--    url: "{{url('/client/services/')}}"+"/"+id,--}}
                        {{--    type: "GET",--}}
                        {{--    data : {'_token' :csrf_token},--}}
                        {{--    success :function (data) {--}}

                        {{--    },--}}
                        {{--    error: function () {--}}
                        {{--        alert("error")--}}
                        {{--    }--}}

                        {{--})--}}
                    // }

                // handling servies modal view

    </script>
@endsection


@endsection
