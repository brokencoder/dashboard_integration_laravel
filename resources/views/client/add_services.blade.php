@extends('layouts.app', ['page' => __('Clients Management'), 'pageSlug' => 'client'])

@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <object class="icon-gradient" data="{{URL::asset('icons/fonts/client_services.svg')}}" style="margin-top: -3px; margin-left: -7px;" type="image/svg+xml" width="50" height="50"></object>
                </div>
                <div>Service Dashboard
                    <div class="page-title-subheading">Information about the services of client
                    </div>
                </div>
            </div>
            <div class="page-title-actions">
                <a href="{{url('client/index')}}" type="button" class="btn-shadow mr-3 btn btn-dark">
                    <i class="fas mr-1 fa-arrow-left"></i>  Back to list
                </a>
            </div>
        </div>
    </div>
    <form id="submit-form" action="{{route('client.send_service_data', ['client_id'=>$client->id])}}" method="POST">
        @csrf
        <input id="row-count" name="row-count" type="hidden" value="0" />
        @if(Session::has('service-errors'))
            <div class="col-10 col-sm-8 col-md-6 col-xl-6 col-lg-6 text-danger  "><h4>{{Session::get('service-errors')}}</h4></div>
        @endif
        <div class="row mb-4 justify-content-around">
            <div id="save-btn" class="col-xl-10 col-lg-10 col-md-10 col-sm-9 col-9" style="visibility: hidden">
                <button type="submit" class="btn btn-primary text-white btn-lg"> <i class="fas fa-lg mr-1 fa-save" aria-hidden="true"></i> Save</button>
            </div>
            <div class="col-xl-2 col-lg-1 col-md-2 col-sm-2 col-2">
                <a type="submit" id="add" class="btn btn-warning text-white"> <i class="fas fa-lg mr-1 fa-plus" aria-hidden="true"></i> ADD</a>
            </div>
        </div>
    </form>
</div>
<div class="app-main__inner">
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-header">All Services
                    <div class="btn-actions-pane-right">
                        <div role="group" class="btn-group-sm btn-group">
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">Sr. No</th>
                            <th class="text-center">Product name</th>
                            <th class="text-center">Free service year</th>
                            <th class="text-center">Service year</th>
                            <th class="text-center">Rate</th>
                            <th class="text-center">Sales date</th>
                            <th class="text-center">Service charge</th>
                            <th class="text-center">Service interval time</th>
                            <th class="text-center">Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @for($i=0; $i<count($services); $i++)
                            @foreach($products as $product)
                                @if($product->id==$services[$i]->products_id)
                                <tr>
                                    <td class="text-center text-muted"># {{$i+1}}</td>
                                    <td>
                                        <div class="widget-content p-0">
                                            <div class="widget-content-wrapper">
                                                <div class="widget-content-left ml-4 mr-3">
                                                    <div class="widget-content-left">
                                                        <img width="40" class="rounded-circle" src="{{URL::asset('images/settings.png')}}" alt="">
                                                    </div>
                                                </div>
                                                <div class="widget-content-left flex2">
                                                    <div class="widget-heading">{{$product->product_name}}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-center">{{$services[$i]->free_service_year}}</td>
                                    <td class="text-center">{{$services[$i]->service_year}}</td>
                                    <td class="text-center">{{$services[$i]->rate}}</td>
                                    <td class="text-center">{{$services[$i]->sales_date}}</td>
                                    <td class="text-center">{{$services[$i]->service_charges}}</td>
                                    <td class="text-center">{{$services[$i]->service_interval_time}}</td>

                                    <td class="text-center">
                                        <a type="button" href="{{route('client.services.edit.display', ['client_id'=>$client_id, 'service   _id'=>$services[$i]->id])}}" class="btn  btn-warning text-white btn-sm"> <i class="fas mr-1 fa-edit" aria-hidden="true"></i> Edit</a>
                                        <a type="button" onclick="delete_client(4)" class="btn btn-danger text-white btn-sm"> <i class="fas fa-lg fa-trash fa-1x mr-1" aria-hidden="true"></i> Delete</a>
                                    </td>
                                 </tr>
                                @endif
                            @endforeach
                        @endfor
                        </tbody>
                    </table>
                </div>
             </div>
        </div>
    </div>

</div>


@section('js_script')
    <script>

        // Append new row function

            $(document).ready(function(){
                var counter = 0;
                $("#add").click(function () {

                    ++counter;

                    $("#submit-form").append(
                    '<div class="tab-content-'+counter+'">'+
                        '<div class="tab-pane tabs-animation fade active show" id="tab-content-0" role="tabpanel">'+
                        '<div class="main-card mb-3 card">'+
                        '<div class="card-body">'+
                        '<div class="row text-white justify-content-around">'+
                        '<h5 class="card-title d-inline col-xl-10 col-lg-10 col-md-10 col-sm-9 col-9">Service No. '+(counter)+'</h5>'+
                    '<a id="delete-btn" class="btn btn-danger col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2 d-inline "><i class="fas mr-1 fa-trash"></i> Delete</a>'+
                    '</div>'+
                    '<div class="form-row mt-3">'+
                        '<div class="col">'+
                        '<div class="form-group">'+
                        '<label class="text-primary" for="product-rate-'+counter+'">Products name</label>'+
                        '<select name="product-name-'+counter+'" class="form-control" id="product-name-'+counter+'">'+
                        @foreach($products as $product)
                        '<option value="{{$product->id}}" class="bg-white">{{$product->product_name}}</option>'+
                        @endforeach
                        '</select>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col">'+
                        '<div class="form-group">'+
                        '<label class="text-primary" for="product-rate-'+counter+'">rate</label>'+
                        '<input type="text" class="form-control" name="product-rate-'+counter+'" id="product-rate-'+counter+'">'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="form-row">'+
                        '<div class="col">'+
                        '<div class="form-group">'+
                        '<label class="text-primary" for="product-service-year-'+counter+'">Service year</label>'+
                        '<input type="text" name="product-service-year-'+counter+'" class="form-control" value=""  id="product-service-year-'+counter+'">'+
                        '</div>'+
                        '</div>'+
                        '<div class="col">'+
                        '<div class="form-group">'+
                        '<label class="text-primary" for="product-free-service-year-'+counter+'">Free service year</label>'+
                        '<input type="text" name="product-free-service-year-'+counter+'" class="form-control" id="product-free-service-year-"'+counter+'">'+
                        '</div>'+
                        '</div>'+
                        '<div class="col">'+
                        '<div class="form-group">'+
                        '<label class="text-primary" for="product-interval-time-'+counter+'">interval time</label>'+
                        '<input type="text" class="form-control" name="product-interval-time-'+counter+'" id="product-interval-time-'+counter+'">'+
                        '</div>'+
                        '</div>'+
                        '</div>'+

                        '<div class="form-row">'+
                        '<div class="col">'+
                        '<div class="form-group">'+
                        '<label class="text-primary" for="product-sales-date-'+counter+'">Sales date</label>'+
                    '<input type="date" class="form-control" name="product-sales-date-'+counter+'" id="product-sales-date-0" >'+
                        '</div>'+
                        '</div>'+
                        '<div class="col">'+
                        '<div class="form-group">'+
                        '<label class="text-primary" for="product-service-charge-'+counter+'">Service charge</label>'+
                    '<input type="text" class="form-control" name="product-service-charge-'+counter+'" id="product-service-charge-'+counter+'">'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</div>');

                    $('#row-count').val(counter)
                    if(counter!=0)
                    {
                        $('#save-btn').css('visibility', 'visible');
                    }
                });
                $(document).on('click', '#delete-btn', function () {

                    $('.tab-content-'+counter).remove();
                    counter--;
                    $('#row-count').val(counter)
                    if(counter==0) {
                        $('#save-btn').css('visibility', 'hidden');
                    }
                });
            });

    </script>
@endsection


@endsection
