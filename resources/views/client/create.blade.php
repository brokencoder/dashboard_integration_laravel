@extends('layouts.app', ['page' => __('Client Management'), 'pageSlug' => 'client'])

@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <object class="icon-gradient" data="{{URL::asset('icons/fonts/client_index_icon.svg')}}" style="margin-top: -3px; margin-left: -7px;" type="image/svg+xml" width="50" height="50"></object>
                    </div>
                    <div>Client Details
                        <div class="page-title-subheading">Add the following client details
                        </div>
                    </div>
                </div>
                <div class="page-title-actions">
                    <a href="{{url('client/index')}}" type="button" class="btn-shadow mr-3 btn btn-dark">
                        <i class="fas mr-1 fa-arrow-left"></i> Back to list
                    </a>
                </div>
            </div>
        </div>
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h4 class="card-title">{{ __('Client Information') }}</h4>
                <p class="card-category">Please fill the following client information</p>
                <form method="POST" action="{{ route('client.store') }}">
                @csrf

                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <div class="form-group mt-2">
                            <label for="first-name-id">First Name</label>
                            <input type="text" value="{{old('first-name')}}" name="first-name" id="first-name-id" class="form-control">
                            @error('first-name')
                              <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md- mb-3">
                        <div class="form-group mt-2">
                            <label for="first-last-id">Last Name</label>
                            <input type="text" value="{{old('last-name')}}" name="last-name" id="first-last-id" class="form-control">
                            @error('last-name')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-12 mb-12">
                        <div class="form-group mt-1">
                            <label class="bmd-label-floating" for="address-id">Address</label>
                            <input type="text"  value="{{old('input-address')}}" name="input-address" id="address-id" class="form-control">
                            @error('input-address')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <div class="form-group mt-1">
                            <label for="phone-number-id">Phone Number</label>
                            <input type="text" value="{{old('input-phone-number')}}" name="input-phone-number" id="phone-number-id" class="form-control">
                            @error('input-phone-number')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group mt-1">
                            <label for="email-id">Email </label>
                            <input type="text" value="{{old('input-email-id')}}" name="input-email-id" id="email-id" class="form-control">
                            @error('input-email-id')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <div class="text-center">
                        <button type="submit" class="btn btn-info mt-4">{{ __('Create client') }}</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
