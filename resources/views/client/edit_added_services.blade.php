@extends('layouts.app', ['page' => __('Clients Management'), 'pageSlug' => 'client'])

@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <object class="icon-gradient" data="{{URL::asset('icons/fonts/client_index_icon.svg')}}" style="margin-top: -3px; margin-left: -7px;" type="image/svg+xml" width="50" height="50"></object>
                </div>
                <div>Service Details
                    <div class="page-title-subheading">Edit the following client service details
                    </div>
                </div>
            </div>
            <div class="page-title-actions">
                <a href="{{url('client/add_services/'.$client_id)}}" type="button" class="btn-shadow mr-3 btn btn-dark">
                    <i class="fas mr-1 fa-arrow-left"></i> Back to list
                </a>
            </div>
        </div>
    </div>

    <div class="main-card mb-3 card">
        <div class="card-body">
            <h4 class="card-title">{{ __('Service Information') }}</h4>

            <p class="card-category">Please fill the following service information</p>
            <form method="POST" action="{{url('/client/'.$client_id.'/services/'.$service->id.'/edit')}}">
                @csrf
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <div class="form-group mt-2">
                            <label class="text-primary" for="product-name-{{$service->id}}">Products name</label>
                            <select name="product-name-{{$service->id}}" class="form-control" id="product-name-{{$service->id}}">
                                @foreach($products as $product)
                                <option value="{{$product->id}}" class="bg-white" @if($service->products_id==$product->id) selected @endif>{{$product->product_name}}</option>
                                @endforeach
                            </select>
                            @error('product-name-'.$service->id)
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="col-md-6 mb-3">
                        <div class="form-group mt-2">
                            <label  class="text-primary" for="product-rate-{{$service->id}}">rate</label>
                            <input type="text" value="{{$service->rate}}" class="form-control" name="product-rate-{{$service->id}}" id="product-rate-{{$service->id}}">
                            @error('product-rate-'.$service->id)
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <div class="form-group mt-2">
                            <div>
                                <label class="text-primary" for="product-service-year-{{$service->id}}">Service year</label>
                                <input type="text" value="{{$service->service_year}}" name="product-service-year-{{$service->id}}" class="form-control"  id="product-service-year-{{$service->id}}" required>
                                @error('product-service-year-'.$service->id)
                                <div class="text-danger">{{$message}}</div>
                                @enderror
                            </div>

                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group mt-2">
                            <label  class="text-primary" for="product-free-service-year-{{$service->id}}">Free service year</label>
                            <input type="text" value="{{$service->free_service_year}}" name="product-free-service-year-{{$service->id}}" class="form-control" id="product-free-service-year-{{$service->id}}" required>
                        </div>
                        @error('product-free-service-year-'.$service->id)
                        <div class="text-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="col">
                        <div class="form-group mt-2">
                            <label  class="text-primary" for="product-interval-time-{{$service->id}}">time</label>
                            <input type="text" value="{{$service->service_interval_time}}" class="form-control" name="product-interval-time-{{$service->id}}" id="product-interval-time-{{$service->id}}" required>
                        </div>
                        @error('product-interval-time-'.$service->id)
                        <div class="text-danger">{{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-row">
                    <div class="col">
                        <div class="form-group mt-1">
                            <label class="text-primary" for="product-sales-date-{{$service->id}}">date</label>
                            <input type="date" value="{{$service->sales_date}}" class="form-control" name="product-sales-date-{{$service->id}}" id="product-sales-date-{{$service->id}}" >
                        </div>
                        @error('product-sales-date-'.$service->id)
                            <div class="text-danger">{{$message}}</div>
                        @enderror
                        </div>
                    <div class="col">
                        <div class="form-group mt-1">
                            <label  class="text-primary" for="product-service-charge-{{$service->id}}" >charge</label>
                            <input type="text" value="{{$service->id}}" class="form-control" name="product-service-charge-{{$service->id}}" id="product-service-charge-{{$service->id}}" >
                            @error('product-service-charge-'.$service->id)
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>

                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <div class="text-center">
                            <button type="submit" class="btn btn-info mt-4">{{ __('Edit') }}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>










@section('edit_service_js')
    <script>

        // Append new row function

            function delete_service(service_id, client_id) {

                var csrf_token = $('meta[name="csrf_token"]').attr('content');
                swal({
                    title: "Delete?",
                    text: "You really like to delete this service ?",
                    icon: "warning",
                    buttons: ["cancel", "Delete"],
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "{{url('client/delete/')}}"+"/"+client_id+"/services/"+service_id,
                                type: "GET",
                                data: {'_token': csrf_token},
                                success: function (data) {
                                    swal("service is deleted", {
                                        icon: "success",
                                        buttons: false,
                                        timer: 3000,
                                    });
                                    location.reload();
                                },
                                error: function () {
                                    alert("error")
                                    swal({
                                        title: 'Opps...',
                                        text: data,
                                        type: 'error',
                                        timer: '1500'
                                    })
                                }
                            })
                        } else {

                        }
                    })
            }

    </script>
@endsection


@endsection
