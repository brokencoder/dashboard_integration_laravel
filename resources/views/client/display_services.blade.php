@extends('layouts.app', ['page' => __('Client Management'), 'pageSlug' => 'client'])

@section('content')
    <div class="container-fluid mt--7">
        <div class="row" >
            <div class="col-xl-12 order-xl-1">
                <div class="card" style="background-color: #f2edee">
                    <div class="card-header" >
                        <div class="container">
                            <div class="tabs tabs-style-topline">
                                <nav>
                                    <ul>
                                        <li><a href="#section-topline-1" class="display-4"><span>Services</span></a></li>
                                        <li><a href="#section-topline-2" class="display-4"><span>Products</span></a></li>
                                        </ul>
                                </nav>
                                <div class="content-wrap">
                                    <section id="section-topline-1">
                                        <div class="container-fluid border-bottom border-dark">
                                            <div class="row justify-content-center mt-4 mb-4">
                                                <h3 class=" text-dark border-bottom border-dark">Service 1</h3>
                                            </div>
                                            <table class="table mb-5" id="custom-table-border-id">
                                                <thead>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <th>Product Name:</th>
                                                    <td>Laptop</td>
                                                    <th>Product Code:</th>
                                                    <td>#454</td>
                                                </tr>
                                                <tr>
                                                    <th>Free Service Year:</th>
                                                    <td>1 year</td>
                                                    <th>rate:</th>
                                                    <td>&#8377 3500</td>
                                                </tr>
                                                <tr>
                                                    <th>Service Year:</th>
                                                    <td>2 year</td>
                                                    <th>Service Charges:</th>
                                                    <td>&#8377 500</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="container-fluid border-bottom border-dark">
                                            <div class="row justify-content-center mt-4  mb-4">
                                                <h3 class=" text-dark border-bottom border-dark">Service 2</h3>
                                            </div>
                                            <table class="table mb-5 " id="custom-table-border-id">
                                                <thead>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <th>Product Name:</th>
                                                    <td>Laptop</td>
                                                    <th>Product Code:</th>
                                                    <td>#454</td>
                                                </tr>
                                                <tr>
                                                    <th>Free Service Year:</th>
                                                    <td>1 year</td>
                                                    <th>rate:</th>
                                                    <td>&#8377 3500</td>
                                                </tr>
                                                <tr>
                                                    <th>Service Year:</th>
                                                    <td>2 year</td>
                                                    <th>Service Charges:</th>
                                                    <td>&#8377 500</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="container-fluid border-bottom border-dark">
                                            <div class="row justify-content-center mt-4 mb-4">
                                                <h3 class=" text-dark border-bottom border-dark">Service 3</h3>
                                            </div>
                                            <table class="table mb-5" id="custom-table-border-id">
                                                <thead>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <th>Product Name:</th>
                                                    <td>Laptop</td>
                                                    <th>Product Code:</th>
                                                    <td>#454</td>
                                                </tr>
                                                <tr>
                                                    <th>Free Service Year:</th>
                                                    <td>1 year</td>
                                                    <th>rate:</th>
                                                    <td>&#8377 3500</td>
                                                </tr>
                                                <tr>
                                                    <th>Service Year:</th>
                                                    <td>2 year</td>
                                                    <th>Service Charges:</th>
                                                    <td>&#8377 500</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </section>
                                    <section id="section-topline-2">


                                    </section>

                                </div><!-- /content -->
                            </div><!-- /tabs -->
                        </div>
                    </div>
                    <div class="card-body mt-4">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('tab_js')
    <script>
        (function() {

            [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
                new CBPFWTabs( el );
            });

        })();
    </script>
@endsection
