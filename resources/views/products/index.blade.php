@extends('layouts.app', ['page' => __('Products Management'), 'pageSlug' => 'products'])

@section('content')
    @extends('layouts.app', ['page' => __('Staff Management'), 'pageSlug' => 'staff'])

@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <object class="icon-gradient" data="{{URL::asset('images/product_home_icon.svg')}}" style="margin-top: -3px; margin-left: -11px;" type="image/svg+xml" width="55" height="80"></object>
                    </div>
                    <div>Product Dashboard
                        <div class="page-title-subheading">information about the product's
                        </div>
                    </div>
                </div>
                <div class="page-title-actions">
                    <a href="{{route('product.create')}}" type="button" class="btn-shadow mr-3 btn btn-dark">
                        <i class="fas mr-2  fa-user-plus"></i>  New Product
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-header">Proudcts list
                        <div class="btn-actions-pane-right">
                            <div role="group" class="btn-group-sm btn-group">
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                            <thead>
                            <tr>
                                <th class="text-center">{{ __('Sr. No') }}</th>
                                <th>{{ __('Product Name') }}</th>
                                <th class="text-center">{{ __('Available') }}</th>
                                <th class="text-center"> {{__('Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(sizeof($products))
                                @for($i=0; $i<count($products); $i++)
                                    <tr>
                                        <td class="text-center text-muted"># {{$i+1}}</td>
                                        <td>
                                            <div class="widget-content p-0">
                                                <div class="widget-content-wrapper">
                                                    <div class="widget-content-left mr-3">
                                                        <div class="widget-content-left">
                                                            <img width="30" class="" src="{{url('images/product_view_icon.png')}}" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="widget-content-left flex2">
                                                        <div class="widget-heading">{{$products[$i]->product_name}}</div>
                                                        {{--                                            <div class="widget-subheading opacity-7">Web Developer</div>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-center">@if($products[$i]->status) Available @else Not Available @endif</td>
                                        <td class="text-center">
                                            <a type="button" href="{{route('product.view', ['product_id'=>$products[$i]->id])}}" class="btn  btn-primary text-white btn-sm"> <i class="fas mr-1 fa-eye" aria-hidden="true"></i> View</a>
                                            <a type="button" href="{{route('product.edit', ['product_id'=>$products[$i]->id] )}}" class="btn  btn-warning text-white btn-sm"> <i class="fas mr-1 fa-edit" aria-hidden="true"></i> Edit</a>
                                            <a type="button" onclick="delete_client({{$products[$i]->id}})" class="btn btn-danger text-white btn-sm"> <i class="fas fa-lg fa-trash fa-1x mr-1" aria-hidden="true"></i> Delete</a>
                                        </td>
                                    </tr>
                                @endfor
                            @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@section('js_script')
    <script>
        // displaying staff data in data-table form
        $('#staff-table').DataTable();


        function  delete_Staff_user(id) {
            var csrf_token=$('meta[name="csrf_token"]').attr('content');
            swal({
                title: "Delete?",
                text: "You really like to delete this staff user ?",
                icon: "warning",
                buttons: ["cancel", "Delete"],
                dangerMode: true,
            })
                .then((willDelete) => {
                    if(willDelete){
                        $.ajax({
                            url : "{{ url('staff/delete')}}"+ "/"+ id,
                            type : "GET",
                            data : {'_token' :csrf_token},
                            success: function(data){

                                swal("Client is deleted", {
                                    icon: "success",
                                    buttons: false,
                                    timer: 3000,
                                });
                                location.reload();


                            },
                            error : function(){
                                swal({
                                    title: 'Opps...',
                                    text : data.message,
                                    type : 'error',
                                    timer : '1500'
                                })
                            }
                        })
                    }else{

                    }
                })
        }
    </script>
@endsection
@endsection



{{--    <div class="row">--}}
{{--        <div class="col-md-12">--}}
{{--            <div class="card ">--}}
{{--                <div class="card-header card-header-primary">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-8">--}}
{{--                            <h4 class="card-title">{{ __('Product\'s Information') }}</h4>--}}
{{--                        </div>--}}
{{--                        <div class="col-4 text-right">--}}
{{--                            <a href="{{ route('products.create') }}" class="btn btn-sm btn-info">{{ __('Add Product') }}</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="card-body mt-2">--}}
{{--                    @include('alerts.success')--}}
{{--                    <div class="table-responsive">--}}
{{--                        <table class="table tablesorter " id="product-table">--}}
{{--                            <thead class=" text-primary">--}}
{{--                            <th scope="col">{{ __('Product Name') }}</th>--}}
{{--                            <th scope="col">{{ __('Product Code') }}</th>--}}
{{--                            <th scope="col">{{ __('Added Date') }}</th>--}}
{{--                            <th scope="col">{{ __('Price') }}</th>--}}
{{--                            <th scope="col">{{ __('Status') }}</th>--}}
{{--                            <th scope="col"> {{__('Action')}}</th>--}}
{{--                            </thead>--}}
{{--                            <tbody>--}}
{{--                        @if(sizeof($products))--}}
{{--                            @foreach ($products as $product)--}}
{{--                                <tr>--}}
{{--                                    <td>{{ $product->product_name }}</td>--}}
{{--                                    <td>{{ $product->product_code }}</td>--}}
{{--                                    <td>{{ $product->product_created_data	 }}</td>--}}
{{--                                    <td>{{ $product->rate }}</td>--}}
{{--                                    <td>{{ $product->status }}</td>--}}
{{--                                    <td class="text-right">--}}
{{--                                        <div class="dropdown">--}}
{{--                                            <a class="btn btn-sm btn-icon-only text-light dropdown-toggle" id="dropdownMenuButton" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                                                <i class="fas fa-ellipsis-v"></i>--}}
{{--                                            </a>--}}
{{--                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">--}}
{{--                                                <a class="dropdown-item" href="{{route('products.modify', ['id'=>$product->id] )}}">Modify</a>--}}
{{--                                                <a class="dropdown-item" href="#" onclick="delete_product({{$product->id}})">Delete</a>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </td>--}}
{{--                                </tr>--}}
{{--                            @endforeach--}}
{{--                            @endif--}}
{{--                            </tbody>--}}

{{--                        </table>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="card-footer py-4">--}}
{{--                    <nav class="d-flex justify-content-end" aria-label="...">--}}
{{--                        {{ $users->links() }}--}}
{{--                    </nav>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

@section('js_script')
@endsection
@endsection
