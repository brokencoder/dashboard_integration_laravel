@extends('layouts.app', ['page' => __('Products Management'), 'pageSlug' => 'products'])

@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <object class="icon-gradient" data="{{URL::asset('images/product_home_icon.svg')}}" style="margin-top: -3px; margin-left: -11px;" type="image/svg+xml" width="55" height="80"></object>
                    </div>
                    <div>Product Dashboard
                        <div class="page-title-subheading">Information about the product
                        </div>
                    </div>
                </div>
                <div class="page-title-actions">
                    <a href="{{url('products/index')}}" type="button" class="btn-shadow mr-3 btn btn-dark">
                        <i class="fas mr-1 fa-arrow-left"></i> Back to list
                    </a>
                </div>
            </div>
        </div>
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h4 class="card-title">{{ __('Product Information') }}</h4>
                <p class="card-category">Please fill the following product information</p>
                <form method="post" action="{{ route('product.store') }}" autocomplete="off">
                @csrf
                <div class="form-row">
                    <div class="col">
                        <div class="form-group mt-2 ">
                            <label class="text-primary" for="product-id">Product name</label>
                            <input type="text" name="product-name" id="product-id" class="form-control">
                            @error('product-name')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group mt-2">
                            <label class="text-primary" for="phone-code-id">Product code</label>
                            <input type="text" name="product-code" id="product-code-id" class="form-control ">
                            @error('product-code')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <div class="form-group mt-4 is-focused">
                            <label class="text-primary " for="product-created-date-id">Date</label>
                            <input type="date" name="product-created-date" id="product-created-date-id" class="form-control" value="">
                            @error('product-created-date')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group mt-4">
                            <label class="text-primary" for="product-rate-id">Product Price</label>
                            <input type="text" name="product-rate" id="product-rate-id" class="form-control">
                            @error('product-rate')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-6">
                        <div class="form-group mt-4">
                            <label class="bmd-label-floating" for="sel1">Availability</label>
                            <select  name="product-availability" class="form-control" id="product-availability-id">
                                <option class="bg-white">Yes</option>
                                <option class="bg-white">No</option>
                            </select>
                            @error('product-availability')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <div class="text-center">
                            <button type="submit" class="btn btn-info mt-4">{{ __('Create product') }}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
