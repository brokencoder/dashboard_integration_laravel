@extends('layouts.app', ['page' => __('Staff Management'), 'pageSlug' => 'staff'])

@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <object class="icon-gradient" data="{{URL::asset('images/product_home_icon.svg')}}" style="margin-top: -3px; margin-left: -11px;" type="image/svg+xml" width="55" height="80"></object>
                    </div>
                    <div>Product Info
                        <div class="page-title-subheading">Information relevant to product
                        </div>
                    </div>
                </div>
                <div class="page-title-actions">
                    <a href="{{url('products/index')}}" type="button" class="btn-shadow mr-3 btn btn-dark">
                        <i class="fas mr-1 fa-arrow-left"></i> Back to list
                    </a>
                </div>
            </div>
        </div>
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="card user-card-full">
                            <div class="row m-l-0 m-r-0">
                                <div class="col-sm-4 bg-c-lite-green user-profile">
                                    <div class="card-block text-center text-white">
                                        <div class="m-b-25">
                                            <img src="{{URL::asset ('images/product_view_icon.png')}}" width="200px" alt="User-Profile-Image">
                                        </div>
                                        <h6 style="font-size: 2rem;">{{$product->product_name}}</h6>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="card-block">
                                        <h6 class="m-b-20 p-b-5 b-b-default f-w-600">Product Information</h6>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p class="m-b-10 f-w-600">Product Code</p>
                                                <h6 class="text-muted f-w-400">{{$product->product_code}}</h6>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="m-b-10 f-w-600">Price</p>
                                                <h6 class="text-muted f-w-400">{{$product->rate}}</h6>
                                            </div>
                                        </div>
                                        <div class="row mt-4">
                                            <div class="col-sm-6">
                                                <p class="m-b-10 f-w-600">Available</p>
                                                <h6 class="text-muted f-w-400">@if($product->status) Yes @else No @endif</h6>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="m-b-10 f-w-600">Added Date </p>
                                                <h6 class="text-muted f-w-400">{{$product->product_created_data}}</h6>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
