@extends('layouts.app', ['page' => __('Staff Management'), 'pageSlug' => 'staff'])

@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <object class="icon-gradient" data="{{URL::asset('fonts/staff_home_icon.svg')}}" style="margin-top: -3px; margin-left: -11px;" type="image/svg+xml" width="65" height="80"></object>
                </div>
                <div>Staff Dashboard
                    <div class="page-title-subheading">Edit the following staff details
                    </div>
                </div>
            </div>
            <div class="page-title-actions">
                <a href="{{url('staff/index')}}" type="button" class="btn-shadow mr-3 btn btn-dark">
                    <i class="fas mr-2  fa-user-plus"></i>  Back to list
                </a>
            </div>
        </div>
    </div>
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h4 class="card-title">{{ __('Service Information') }}</h4>

            <p class="card-category">Please fill the following service information</p>
            <form method="POST" action="{{ url('staff/update/'.$staff->id) }}">
                @csrf
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <div class="form-group mt-2 ">
                            <label class="text-primary" for="first-name-id">First Name</label>
                            <input type="text" name="first-name" value="{{$staff->first_name}}" id="first-name-id" class="form-control">
                            @error('first-name')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group mt-2 ">
                            <label class="text-primary" for="first-last-id">Last Name</label>
                            <input type="text" name="last-name" value="{{$staff->last_name}}" id="first-last-id" class="form-control">
                            @error('last-name')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <div class="form-group mt-4">
                            <label class="text-primary" for="address-id">Address</label>
                            <input type="text" name="input-address" value="{{$staff->address}}" id="address-id" class="form-control">
                            @error('input-address')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <div class="form-group mt-4 ">
                            <label class="text-primary" for="phone-number-id">Phone Number</label>
                            <input type="text" name="input-phone-number" value="{{$staff->phone_number}}" id="phone-number-id" class="form-control">
                            @error('input-phone-number')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group mt-4 ">
                            <label class="text-primary" for="email-id">Email </label>
                            <input type="text" name="input-email-id" id="email-id" value="{{$staff->email}}" class="form-control ">
                            @error('input-email-id')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="text-center">
                            <button type="submit" class="btn btn-info mt-4">{{ __('Edit') }}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
