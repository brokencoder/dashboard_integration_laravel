@extends('layouts.app', ['page' => __('Staff Management'), 'pageSlug' => 'staff'])

@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <object class="icon-gradient" data="{{URL::asset('fonts/staff_home_icon.svg')}}" style="margin-top: -3px; margin-left: -11px;" type="image/svg+xml" width="65" height="80"></object>
                    </div>
                    <div>Staff Dashboard
                        <div class="page-title-subheading">information about the staff's
                        </div>
                    </div>
                </div>
                <div class="page-title-actions">
                    <a href="{{url('staff/index')}}" type="button" class="btn-shadow mr-3 btn btn-dark">
                        <i class="fas mr-1 fa-arrow-left"></i>  Back to list
                    </a>
                </div>
            </div>
        </div>
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h4 class="card-title">{{ __('Staff Information') }}</h4>
                <p class="card-category">Please fill the following staff information</p>
                <form method="POST" action="{{ route('staff.store') }}" >
                @csrf

                <div class="row">
                    <div class="col ">
                        <div class="form-group mt-2 ">
                            <label class="text-primary" for="first-name-id">First Name</label>
                            <input type="text" value="{{old('first-name')}}" name="first-name" id="first-name-id" class="form-control">
                            @error('first-name')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group mt-2 ">
                            <label class="text-primary" for="last-name-id">Last Name</label>
                            <input type="text" value="{{old('last-name')}}" name="last-name" id="last-name-id" class="form-control">
                            @error('last-name')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group mt-2">
                            <label class="text-primary" for="address-id">Address</label>
                            <input type="text" value="{{old('input-address')}}" name="input-address" id="address-id" class="form-control">
                            @error('input-address')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group mt-2">
                            <label class="text-primary" for="phone-number-id">Phone Number</label>
                            <input type="text" value="{{old('input-phone-number')}}" name="input-phone-number" id="phone-number-id" class="form-control">
                            @error('input-phone-number')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group mt-2">
                            <label class="text-primary" for="email-id">Email </label>
                            <input type="text" value="{{old('input-email-id')}}" name="input-email-id" id="email-id" class="form-control ">
                            @error('input-email-id')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="text-center">
                            <button type="submit" class="btn btn-info mt-4">{{ __('Create staff') }}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
