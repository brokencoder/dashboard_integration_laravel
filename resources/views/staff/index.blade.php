@extends('layouts.app', ['page' => __('Staff Management'), 'pageSlug' => 'staff'])

@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <object class="icon-gradient" data="{{URL::asset('fonts/staff_home_icon.svg')}}" style="margin-top: -3px; margin-left: -11px;" type="image/svg+xml" width="65" height="80"></object>
                </div>
                <div>Staff Dashboard
                    <div class="page-title-subheading">information about the staff's
                    </div>
                </div>
            </div>
            <div class="page-title-actions">
                <a   href="{{url('staff/create')}}" type="button" class="btn-shadow mr-3 btn btn-dark">
                    <i class="fas mr-2  fa-user-plus"></i>  New Staff
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-header">Active Clients
                    <div class="btn-actions-pane-right">
                        <div role="group" class="btn-group-sm btn-group">
                           </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">{{ __('Sr. No') }}</th>
                            <th>{{ __('Full Name') }}</th>
                            <th class="text-center">{{ __('Phone') }}</th>
                            <th class="text-center"> {{__('Action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(sizeof($staffs))
                            @for($i=0; $i<count($staffs); $i++)
                                <tr>
                                    <td class="text-center text-muted"># {{$i+1}}</td>
                                    <td>
                                        <div class="widget-content p-0">
                                            <div class="widget-content-wrapper">
                                                <div class="widget-content-left mr-3">
                                                    <div class="widget-content-left">
                                                        <img width="30" class="" src="{{url('images/staff_id-card.png')}}" alt="">
                                                    </div>
                                                </div>
                                                <div class="widget-content-left flex2">
                                                    <div class="widget-heading">{{$staffs[$i]->first_name.' '.$staffs[$i]->last_name }}</div>
                                                    {{--                                            <div class="widget-subheading opacity-7">Web Developer</div>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-center">{{$staffs[$i]->phone_number}}</td>
                                    {{--                            <td class="text-center">--}}
                                    {{--                                <div class="badge badge-warning">Pending</div>--}}
                                    {{--                            </td>--}}
                                    <td class="text-center">
                                        <a type="button" href="{{route('staff.view', ['client_id'=>$staffs[$i]->id])}}" class="btn  btn-primary text-white btn-sm"> <i class="fas mr-1 fa-eye" aria-hidden="true"></i> View</a>
                                        <a type="button" href="{{route('staff.edit', ['staff_id'=>$staffs[$i]->id] )}}" class="btn  btn-warning text-white btn-sm"> <i class="fas mr-1 fa-edit" aria-hidden="true"></i> Edit</a>
                                        <a type="button" onclick="delete_client({{$staffs[$i]->id}})" class="btn btn-danger text-white btn-sm"> <i class="fas fa-lg fa-trash fa-1x mr-1" aria-hidden="true"></i> Delete</a>
                                    </td>
                                </tr>
                            @endfor
                        @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>




    @section('js_script')
        <script>
            // displaying staff data in data-table form
                    $('#staff-table').DataTable();


                    function  delete_Staff_user(id) {
                        var csrf_token=$('meta[name="csrf_token"]').attr('content');
                        swal({
                            title: "Delete?",
                            text: "You really like to delete this staff user ?",
                            icon: "warning",
                            buttons: ["cancel", "Delete"],
                            dangerMode: true,
                        })
                        .then((willDelete) => {
                            if(willDelete){
                                $.ajax({
                                    url : "{{ url('staff/delete')}}"+ "/"+ id,
                                    type : "GET",
                                    data : {'_token' :csrf_token},
                                    success: function(data){

                                        swal("Client is deleted", {
                                            icon: "success",
                                            buttons: false,
                                            timer: 3000,
                                        });
                                        location.reload();


                                    },
                                    error : function(){
                                        swal({
                                            title: 'Opps...',
                                            text : data.message,
                                            type : 'error',
                                            timer : '1500'
                                        })
                                    }
                                })
                            }else{

                            }
                        })
                    }
        </script>
    @endsection
@endsection
