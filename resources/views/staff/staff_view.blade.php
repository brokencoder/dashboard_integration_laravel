@extends('layouts.app', ['page' => __('Staff Management'), 'pageSlug' => 'staff'])

@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <object class="icon-gradient" data="{{URL::asset('fonts/staff_home_icon.svg')}}" style="margin-top: -3px; margin-left: -11px;" type="image/svg+xml" width="65" height="80"></object>
                    </div>
                    <div>Staff Profile
                        <div class="page-title-subheading">Information relevant to staff
                        </div>
                    </div>
                </div>
                <div class="page-title-actions">
                    <a href="{{url('staff/index')}}" type="button" class="btn-shadow mr-3 btn btn-dark">
                        <i class="fas mr-1 fa-arrow-left"></i> Back to list
                    </a>
                </div>
            </div>
        </div>
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="card user-card-full">
                            <div class="row m-l-0 m-r-0">
                                <div class="col-sm-4 bg-c-lite-green user-profile">
                                    <div class="card-block text-center text-white">
                                        <div class="m-b-25">
                                            <img src="{{URL::asset ('images/staff_view_icon.png')}}" width="200px" alt="User-Profile-Image">
                                        </div>
                                        <h6 style="font-size: 2rem;">{{$staff->first_name.' '.$staff->last_name}}</h6>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="card-block">
                                        <h6 class="m-b-20 p-b-5 b-b-default f-w-600">Personal Information</h6>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p class="m-b-10 f-w-600">Email</p>
                                                <h6 class="text-muted f-w-400">{{$staff->email}}</h6>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="m-b-10 f-w-600">Phone</p>
                                                <h6 class="text-muted f-w-400">{{$staff->phone_number}}</h6>
                                            </div>
                                        </div>
                                        <div class="row mt-4">
                                            <div class="col-sm-6">
                                                <p class="m-b-10 f-w-600">Address</p>
                                                <h6 class="text-muted f-w-400">{{$staff->address}}</h6>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="m-b-10 f-w-600">Created at</p>
                                                <h6 class="text-muted f-w-400">{{$staff->created_at}}</h6>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
