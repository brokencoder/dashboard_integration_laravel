<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
            <span>
                <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                    <span class="btn-icon-wrapper">
                        <i class="fa fa-ellipsis-v fa-w-6"></i>
                    </span>
                </button>
            </span>
    </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">Dashboards</li>
                <li>
                    <a href="{{URL::asset('/dashboard')}}" @if(str_contains(Request::url(), '/dashboard')) class="mm-active" @endif >
                        <i class="metismenu-icon">
                            <object data="{{URL::asset('icons/fonts/diamond.svg')}}" style="margin-top: 5px;" type="image/svg+xml" width="25" height="25"></object>
                        </i>
                        Main Dashboard
                    </a>
                </li>
                <li class="app-sidebar__heading">Productivity</li>
                <li>
                    <a href="#" >
                        <i class="metismenu-icon @if(str_contains(Request::url(), '/client/index')) mm-active @endif">
                            <object data="{{URL::asset('icons/fonts/users.svg')}}" style="margin-top: 5px;" type="image/svg+xml" width="25" height="25"></object>
                        </i>
                        Clients
                        <i class="metismenu-state-icon caret-left">
                            <object data="{{URL::asset('icons/fonts/angle-down.svg')}}" style="margin-top: 5px;" type="image/svg+xml" width="25" height="25"></object>
                        </i>
                    </a>
                    <ul class="@if(str_contains(Request::url(), '/client')) mm-show @endif">
                        <li>
                            <a href="{{url('/client/index')}}" @if(str_contains(Request::url(), '/client/index')) class="mm-active" @endif>
                                <i class="metismenu-icon"></i>
                                View
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/client/create')}}" @if(str_contains(Request::url(), '/client/create')) class="mm-active" @endif>
                                <i class="metismenu-icon"></i>
                                Create
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" >
                        <i class="metismenu-icon ">
                            <object data="{{URL::asset('icons/fonts/portfolio.svg')}}" style="margin-top: 5px;" type="image/svg+xml" width="25" height="25"></object>
                        </i>
                        Staff's
                        <i class="metismenu-state-icon caret-left">
                            <object data="{{URL::asset('icons/fonts/angle-down.svg')}}" style="margin-top: 5px;" type="image/svg+xml" width="25" height="25"></object>
                        </i>
                    </a>
                    <ul class="@if(str_contains(Request::url(), '/staff')) mm-show @endif">
                        <li>
                            <a href="{{route('staff.index')}}" @if(str_contains(Request::url(), '/staff/index')) class="mm-active" @endif>
                                <i class="metismenu-icon">
                                </i>View
                            </a>
                        </li>
                        <li>
                            <a href="{{route('staff.create')}}" @if(str_contains(Request::url(), '/staff/create')) class="mm-active" @endif>
                                <i class="metismenu-icon">
                                </i>
                                Create
                            </a>
                        </li>

                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon">
                            <object data="{{URL::asset('icons/fonts/product.svg')}}" style="margin-top: 5px;" type="image/svg+xml" width="25" height="25"></object>
                        </i>
                        Products
                        <i class="metismenu-state-icon caret-left">
                            <object data="{{URL::asset('icons/fonts/angle-down.svg')}}" style="margin-top: 5px;" type="image/svg+xml" width="25" height="25"></object>
                        </i>
                    </a>
                    <ul class="@if(str_contains(Request::url(), '/product')) mm-show @endif">
                        <li>
                            <a href="{{url('product/create')}}" @if(str_contains(Request::url(), '/products/create')) class="mm-active" @endif>
                                <i class="metismenu-icon">
                                </i>
                                Create
                            </a>
                        </li>
                        <li>
                            <a href="{{url('products/index')}}" @if(str_contains(Request::url(), '/products/index')) class="mm-active" @endif>
                                <i class="metismenu-icon">
                                </i>View
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
