<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title> This is an example dashboard created using build-in elements and components.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">
    <meta name="msapplication-tap-highlight" content="no">
    <link rel="stylesheet" href="{{URL::asset('css/icons/css/pe_icon_stroke.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/client_view.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/icons/css/helper.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/icons/css/Pe-icon-7-stroke.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/icons/css/v4-shims.min.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">
    <link href="{{URL::asset('css/main.css')}}" rel="stylesheet">
{{--    <script src="{{URL::asset('scripts/jquery.js')}}"></script>--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
    <body>
        @auth()
            <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
                @include('layouts.navbars.navbar')
                <div class="app-main">
                    @include('layouts.navbars.sidebar')
                    <div class="app-main__outer">
                        @yield('content')
                        @include('layouts.footer')
                    </div>
                </div>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        @else
            @include('layouts.navbars.navbar')
            <div class="wrapper wrapper-full-page">
                <div class="full-page {{ $contentClass ?? '' }}">
                    <div class="content">
                        <div class="container">
                            @yield('content')
                        </div>
                    </div>
                    @include('layouts.footer')
                </div>
            </div>
        @endauth
{{--        <div class="fixed-plugin">--}}
{{--            <div class="dropdown show-dropdown">--}}
{{--                <a href="#" data-toggle="dropdown">--}}
{{--                    <i class="fa fa-cog fa-2x"> </i>--}}
{{--                </a>--}}
{{--                <ul class="dropdown-menu">--}}
{{--                    <li class="header-title"> Sidebar Filters</li>--}}
{{--                    <li class="adjustments-line">--}}
{{--                        <a href="javascript:void(0)" class="switch-trigger active-color">--}}
{{--                            <div class="badge-colors ml-auto mr-auto">--}}
{{--                                <span class="badge filter badge-purple" data-color="purple"></span>--}}
{{--                                <span class="badge filter badge-azure" data-color="azure"></span>--}}
{{--                                <span class="badge filter badge-green" data-color="green"></span>--}}
{{--                                <span class="badge filter badge-warning" data-color="orange"></span>--}}
{{--                                <span class="badge filter badge-danger" data-color="danger"></span>--}}
{{--                                <span class="badge filter badge-rose active" data-color="rose"></span>--}}
{{--                            </div>--}}
{{--                            <div class="clearfix"></div>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="header-title">Images</li>--}}
{{--                    <li class="active">--}}
{{--                        <a class="img-holder switch-trigger" href="javascript:void(0)">--}}
{{--                            <img src="../assets/img/sidebar-1.jpg" alt="">--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a class="img-holder switch-trigger" href="javascript:void(0)">--}}
{{--                            <img src="../assets/img/sidebar-2.jpg" alt="">--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a class="img-holder switch-trigger" href="javascript:void(0)">--}}
{{--                            <img src="../assets/img/sidebar-3.jpg" alt="">--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a class="img-holder switch-trigger" href="javascript:void(0)">--}}
{{--                            <img src="../assets/img/sidebar-4.jpg" alt="">--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="button-container">--}}
{{--                        <a href="https://www.creative-tim.com/product/material-dashboard" target="_blank" class="btn btn-primary btn-block">Free Download</a>--}}
{{--                    </li>--}}
{{--                    <!-- <li class="header-title">Want more components?</li>--}}
{{--                        <li class="button-container">--}}
{{--                            <a href="https://www.creative-tim.com/product/material-dashboard-pro" target="_blank" class="btn btn-warning btn-block">--}}
{{--                              Get the pro version--}}
{{--                            </a>--}}
{{--                        </li> -->--}}
{{--                    <li class="button-container">--}}
{{--                        <a href="https://demos.creative-tim.com/material-dashboard/docs/2.1/getting-started/introduction.html" target="_blank" class="btn btn-default btn-block">--}}
{{--                            View Documentation--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="button-container github-star">--}}
{{--                        <a class="github-button" href="https://github.com/creativetimofficial/material-dashboard" data-icon="octicon-star" data-size="large" data-show-count="true" aria-label="Star ntkme/github-buttons on GitHub">Star</a>--}}
{{--                    </li>--}}
{{--                    <li class="header-title">Thank you for 95 shares!</li>--}}
{{--                    <li class="button-container text-center">--}}
{{--                        <button id="twitter" class="btn btn-round btn-twitter"><i class="fa fa-twitter"></i> &middot; 45</button>--}}
{{--                        <button id="facebook" class="btn btn-round btn-facebook"><i class="fa fa-facebook-f"></i> &middot; 50</button>--}}
{{--                        <br>--}}
{{--                        <br>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        </div>--}}
        @stack('notfication_js')
        @stack('js')
        <script type="text/javascript" src="{{URL::asset('scripts/main.js')}}"></script>
    </body>

        <script>

             $(document).ready(function() {
                 $().ready(function() {


                    {{--// AJax for displaying client data in datatable--}}
                    {{--$('#client-table').DataTable({--}}
                    {{--    processing: true,--}}
                    {{--    serverSide: true,--}}
                    {{--    ajax: '{{ url('display_client_data') }}',--}}

                    {{--    columns: [--}}
                    {{--        { data: 'first_name', name:'first_name' },--}}
                    {{--        { data: 'last_name', name: 'last_name' },--}}
                    {{--        { data: 'email', name: 'email' },--}}
                    {{--        { data: 'address', name: 'address'},--}}
                    {{--        { data: 'phone_number', name: 'phone_number' },--}}
                    {{--        {--}}
                    {{--            data:'id',--}}
                    {{--            "mRender": function(data, type, full) {--}}
                    {{--                return '' +--}}
                    {{--                    '<div class="dropdown">' +--}}
                    {{--                    '<a class="btn btn-sm btn-icon-only text-light dropdown-toggle" id="dropdownMenuButton" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-ellipsis-v"></i> </a>' +--}}
                    {{--                    '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">' +--}}
                    {{--                    '<a class="dropdown-item" href="'+'modify/'+data+'/'+'">Modify</a>' +--}}
                    {{--                    '<a class="dropdown-item" onclick="'+delete_staff()+'">Delete</a>' +--}}
                    {{--                    '<a class="dropdown-item" >Add Staff</a>' +--}}
                    {{--                    '</div>';--}}
                    {{--            }--}}
                    {{--        }--}}
                    {{--    ],--}}
                    {{--});--}}

                    {{--function delete_staff()--}}
                    {{--{--}}
                    {{--    swal("You really want to delete this user",{--}}
                    {{--        button: "Delete",--}}
                    {{--    });--}}
                    {{--}--}}

                    // AJax for displying product data in datatable form
                    {{--$('#product-table').DataTable({--}}
                    {{--    processing: true,--}}
                    {{--    serverSide: true,--}}
                    {{--    ajax: '{{ url('display_product_data') }}',--}}

                    {{--    columns: [--}}
                    {{--        { data: 'product_name' },--}}
                    {{--        { data: 'product_code'},--}}
                    {{--        { data: 'product_created_data'},--}}
                    {{--        { data: 'rate'},--}}
                    {{--        { data: 'status',--}}
                    {{--          "mRender":function (data, type, full) {--}}
                    {{--            if(data=="1"){--}}
                    {{--                return "Available"--}}
                    {{--            }--}}
                    {{--            else--}}
                    {{--            {--}}
                    {{--                return "Not available"--}}
                    {{--            }--}}
                    {{--          }--}}
                    {{--        },--}}
                    {{--        {--}}
                    {{--            data:'id',--}}
                    {{--            "mRender": function(data, type, full) {--}}
                    {{--                return '' +--}}
                    {{--                    '<div class="dropdown">' +--}}
                    {{--                    '<a class="btn btn-sm btn-icon-only text-light dropdown-toggle" id="dropdownMenuButton" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-ellipsis-v"></i> </a>' +--}}
                    {{--                    '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">' +--}}
                    {{--                    '<a class="dropdown-item" href="'+'modify/'+data+'/'+'">Modify</a>' +--}}
                    {{--                    '<a class="dropdown-item" href="'+'delete/'+data+'/'+'">Delete</a>' +--}}
                    {{--                    '</div>';--}}
                    {{--            }--}}
                    {{--        }--}}
                    {{--    ],--}}
                    {{--});--}}

                    // AJax for displaying staff data in datatable

                    {{--$('#staff-table').DataTable({--}}
                    {{--    processing: true,--}}
                    {{--    serverSide: true,--}}
                    {{--    ajax: '{{ url('display_staff_data') }}',--}}

                    {{--    columns: [--}}
                    {{--        { data: 'first_name', name:'first_name' },--}}
                    {{--        { data: 'last_name', name: 'last_name' },--}}
                    {{--        { data: 'email', name: 'email' },--}}
                    {{--        { data: 'address', name: 'address'},--}}
                    {{--        { data: 'phone_number', name: 'phone_number' },--}}
                    {{--        {--}}
                    {{--            data:'id',--}}
                    {{--            "mRender": function(data, type, full) {--}}
                    {{--                return '' +--}}
                    {{--                    '<div class="dropdown">' +--}}
                    {{--                    '<a class="btn btn-sm btn-icon-only text-light dropdown-toggle" id="dropdownMenuButton" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-ellipsis-v"></i> </a>' +--}}
                    {{--                    '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">' +--}}
                    {{--                    '<a class="dropdown-item" href="'+'modify/'+data+'/'+'">Modify</a>' +--}}
                    {{--                    '<a class="dropdown-item" href="'+'delete/'+data+'/'+'">Delete</a>' +--}}
                    {{--                    '</div>';--}}
                    {{--            }--}}
                    {{--        }--}}
                    {{--    ],--}}
                    {{--});--}}
                    {{--function deleteFunction(data){--}}
                    {{--    alert("asdf")--}}
                    {{--}--}}

                    $('.dropdown-toggle').dropdown('update');
                    $('.dropdown-toggle').dropdown('dispose');
                    $sidebar = $('.sidebar');
                    $navbar = $('.navbar');
                    $main_panel = $('.main-panel');

                    $full_page = $('.full-page');

                    $sidebar_responsive = $('body > .navbar-collapse');
                    sidebar_mini_active = true;
                    white_color = false;

                    window_width = $(window).width();

                    fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

                    $('.fixed-plugin a').click(function(event) {
                        if ($(this).hasClass('switch-trigger')) {
                            if (event.stopPropagation) {
                                event.stopPropagation();
                            } else if (window.event) {
                                window.event.cancelBubble = true;
                            }
                        }
                    });

                    $('.fixed-plugin .background-color span').click(function() {
                        $(this).siblings().removeClass('active');
                        $(this).addClass('active');

                        var new_color = $(this).data('color');

                        if ($sidebar.length != 0) {
                            $sidebar.attr('data', new_color);
                        }

                        if ($main_panel.length != 0) {
                            $main_panel.attr('data', new_color);
                        }

                        if ($full_page.length != 0) {
                            $full_page.attr('filter-color', new_color);
                        }

                        if ($sidebar_responsive.length != 0) {
                            $sidebar_responsive.attr('data', new_color);
                        }
                    });

                    $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function() {
                        var $btn = $(this);

                        if (sidebar_mini_active == true) {
                            $('body').removeClass('sidebar-mini');
                            sidebar_mini_active = false;
                            blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
                        } else {
                            $('body').addClass('sidebar-mini');
                            sidebar_mini_active = true;
                            blackDashboard.showSidebarMessage('Sidebar mini activated...');
                        }

                        // we simulate the window Resize so the charts will get updated in realtime.
                        var simulateWindowResize = setInterval(function() {
                            window.dispatchEvent(new Event('resize'));
                        }, 180);

                        // we stop the simulation of Window Resize after the animations are completed
                        setTimeout(function() {
                            clearInterval(simulateWindowResize);
                        }, 1000);
                    });

                    $('.switch-change-color input').on("switchChange.bootstrapSwitch", function() {
                            var $btn = $(this);

                            if (white_color == true) {
                                $('body').addClass('change-background');
                                setTimeout(function() {
                                    $('body').removeClass('change-background');
                                    $('body').removeClass('white-content');
                                }, 900);
                                white_color = false;
                            } else {
                                $('body').addClass('change-background');
                                setTimeout(function() {
                                    $('body').removeClass('change-background');
                                    $('body').addClass('white-content');
                                }, 900);

                                white_color = true;
                            }
                    });


               });
            });
        </script>
        @yield('tab_js')
        @yield('js_script')
        @yield('edit_service_js')
        @yield('notification_js')
        @stack('js')
    </body>
</html>
