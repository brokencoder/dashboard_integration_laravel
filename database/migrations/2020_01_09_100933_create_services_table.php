<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class   CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('clients_id');
            $table->foreign('clients_id')->references('id')->on('clients')->onDelete('cascade');
            $table->string('product_name');
            $table->unsignedBigInteger('free_service_year');
            $table->unsignedBigInteger('service_year');
            $table->unsignedBigInteger('rate');
            $table->date('sales_date');
            $table->unsignedBigInteger('service_charges');
            $table->unsignedBigInteger('service_interval_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
